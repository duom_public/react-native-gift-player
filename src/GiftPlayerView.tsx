import React, { useRef, forwardRef, useImperativeHandle } from 'react';
import {
  requireNativeComponent,
  findNodeHandle,
  UIManager,
  Platform,
  ViewProps,
} from 'react-native';

interface IGiftPlayerProps extends ViewProps {
  source?: string;
}

const moduleName = 'RTCGiftPlayerView';

const safeGetViewManagerConfig = () => {
  if (UIManager.getViewManagerConfig) {
    // RN >= 0.58
    return UIManager.getViewManagerConfig(moduleName);
  }
  // RN < 0.58
  const mgr: Record<string, { Commands: { [key: string]: string } }> =
    UIManager as any;
  return mgr[moduleName];
};

const RTCGiftPlayerView = requireNativeComponent(moduleName);

export interface TGiftPlayerMethod{
  startPlay: (path:string)=>void;
  stopPlay: ()=>void;
}

type TGiftPlayerRef = React.ForwardedRef<TGiftPlayerMethod>

export const GiftPlayerView = forwardRef((props: IGiftPlayerProps, giftPlayRef: TGiftPlayerRef) => {
    const ref = useRef(null);
    useImperativeHandle(
      giftPlayRef,
      () => {
        const runCommand = (name: string, args: string[]) => {
          const handle = findNodeHandle(ref.current);
          if (!handle) {
            return null;
          }
          console.log('runCommand');
          let cmd = name;
          if (Platform.OS === 'android') {
            cmd = safeGetViewManagerConfig().Commands[name].toString();
          }
          return UIManager.dispatchViewManagerCommand(handle, cmd, args);
        };
        const startPlay = (path: string) => {
          console.log('startPlay');
          runCommand('play', [path]);
        };
        const stopPlay = () => {
          console.log('stopPlay');
          runCommand('stop', []);
        };
        return {
          startPlay,
          stopPlay,
        };
      },
      [],
    );

    return <RTCGiftPlayerView ref={ref} {...props} />;
  },
);
