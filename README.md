# react-native-gift-player

## Getting started

`$ npm install react-native-gift-player --save`

### Mostly automatic installation

`$ react-native link react-native-gift-player`

## Usage
```javascript
import { ReactNativeGiftPlayer, GiftPlayerView } from 'react-native-gift-player';

<GiftPlayerView ref={ref} style={{width: 400, height: 700}} />

startPlay: 开始播放 ref.current?.startPlay("ffffff");

stopPlay: 停止播放 ref.current?.stopPlay();


```
