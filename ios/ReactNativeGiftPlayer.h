// ReactNativeGiftPlayer.h

#import <React/RCTViewManager.h>
#import <React/RCTBridgeModule.h>

@interface ReactNativeGiftPlayer : RCTViewManager <RCTBridgeModule>

@end
