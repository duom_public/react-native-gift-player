//
//  GiftPlayerView.m
//  react-native-gift-player
//
//  Created by kuroky on 2023/3/31.
//

#import "GiftPlayerView.h"
#import "YYEVA.h"

@interface GiftPlayerView()<IYYEVAPlayerDelegate>

@property (nonatomic, strong) YYEVAPlayer *player;

@end

@implementation GiftPlayerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame: frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.player];
        self.player.frame = frame;
    }
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.player];
    }
    return self;
}

-(void)startPlay:(NSString *)path {
    if (path.length <= 0) {
        NSLog(@"播放资源为空！！！");
        return;
    }
    
    [self.player play:path repeatCount:1];
}

-(void)stopPlay {
    [self.player stopAnimation];
}

- (void)setSource:(NSString *)source {
    [self startPlay:source];
}

-(YYEVAPlayer *)player {
    if (!_player) {
        _player = [[YYEVAPlayer alloc] init];
        _player.delegate=self;
    }
    return _player;
}

-(void)layoutSubviews{
    self.player.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [super layoutSubviews];
}

- (void)evaPlayer:(YYEVAPlayer *)player playFail:(NSError *)error{
    NSLog(@"Player error: %@", error);
}

@end
