// ReactNativeGiftPlayer.m

#import "ReactNativeGiftPlayer.h"
#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
//#import "YYEVA.h"
#import "GiftPlayerView.h"

@interface ReactNativeGiftPlayer()

@end

@implementation ReactNativeGiftPlayer

RCT_EXPORT_MODULE(RTCGiftPlayerView)

-(UIView *)view {
    GiftPlayerView* view = [[GiftPlayerView alloc] init];
    return view;
}

RCT_EXPORT_VIEW_PROPERTY(source, NSString);

RCT_EXPORT_METHOD(play:(nonnull NSNumber*) reactTag withFile:(NSString*)file) {
    [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
        GiftPlayerView *view = viewRegistry[reactTag];
        if (!view || ![view isKindOfClass:[GiftPlayerView class]]) {
            RCTLogError(@"Cannot find NativeView with tag #%@", reactTag);
            return;
        }
        [view startPlay:file];
    }];
}

RCT_EXPORT_METHOD(stop:(nonnull NSNumber*) reactTag) {
    [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
        GiftPlayerView *view = viewRegistry[reactTag];
        if (!view || ![view isKindOfClass:[GiftPlayerView class]]) {
            RCTLogError(@"Cannot find NativeView with tag #%@", reactTag);
            return;
        }
        [view stopPlay];
    }];
}

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

@end
