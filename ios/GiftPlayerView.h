//
//  GiftPlayerView.h
//  react-native-gift-player
//
//  Created by kuroky on 2023/3/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GiftPlayerView : UIView

@property (nonatomic, copy) NSString *source;

-(void)startPlay:(NSString *)path;

-(void)stopPlay;

@end

NS_ASSUME_NONNULL_END
