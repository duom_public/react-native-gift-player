package com.giftplayer.util;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FileUtil {

    /**
     * 判断路径是否存在
     *
     * @param path 需要判断的路径
     * @return true 是存在，false 是不存在
     */
    public static boolean isPathExist(String path) {
        File file = new File(path);
        return file.exists();
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    public static String getImgDir(Context context, String child) {
        File externalFilesDir = context.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), child);
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 拷贝资源文件到存储
     *
     * @param context
     * @param giftName
     * @param ASSETS_NAME
     */
    public static void copyAssetsToStorage(Context context, String giftName, String ASSETS_NAME) {
        String savePath = getImgDir(context, "VideoAssets") + giftName;
        if (isPathExist(savePath)) {
            return;
        }
        try {
            InputStream is = context.getResources().getAssets().open(ASSETS_NAME);
            FileOutputStream fos = null;
            File f = new File(savePath);
            fos = new FileOutputStream(f);
            byte[] buffer = new byte[7168];
            int count = 0;
            while ((count = is.read(buffer)) > 0) {
                fos.write(buffer, 0, count);
            }
            fos.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
