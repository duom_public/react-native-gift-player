package com.giftplayer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.giftplayer.view.GiftPlayerView;

import java.util.Map;

public class GiftPlayerManager extends SimpleViewManager<GiftPlayerView> implements LifecycleEventListener {

    private final static int COMMAND_PLAY_GIFT = 1;//播放礼物动效
    private final static int COMMAND_PLAY_STOP = 2;//停止播放
    private final static int COMMAND_DESTROY = 3;//销毁播放

    private ReactContext mReactContext;
    private GiftPlayerView mGiftPlayerView;


    @NonNull
    @Override
    public String getName() {
        return "RTCGiftPlayerView";
    }

    public GiftPlayerManager(ReactApplicationContext reactContext) {
        this.mReactContext = reactContext;
        mReactContext.addLifecycleEventListener(this);
    }

    @NonNull
    @Override
    protected GiftPlayerView createViewInstance(@NonNull ThemedReactContext reactContext) {
        if (reactContext.hasCurrentActivity()) {
            return mGiftPlayerView = new GiftPlayerView(reactContext);
        }
        return null;
    }

    @Override
    public void onDropViewInstance(@NonNull GiftPlayerView view) {
        view.onVideoDestroy();
    }

    @ReactProp(name = "source")
    public void setPath(GiftPlayerView giftPlayerView, String source) {
        giftPlayerView.startPlay(source);
    }

    @Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of("play", COMMAND_PLAY_GIFT, "stop", COMMAND_PLAY_STOP, "destroy", COMMAND_DESTROY);
    }

    @Override
    public void receiveCommand(@NonNull GiftPlayerView root, String commandId, @Nullable ReadableArray args) {
        int commandIdInt = Integer.parseInt(commandId);
        switch (commandIdInt) {
            case COMMAND_PLAY_GIFT:
                if (args != null) {
                    String path = args.getString(0);
                    root.startPlay(path);
                }
                break;
            case COMMAND_PLAY_STOP:
                root.stopPlay();
            case COMMAND_DESTROY:
                root.onVideoDestroy();
            default:
                break;
        }
    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {
        if (mGiftPlayerView != null) {
            mGiftPlayerView.stopPlay();
        }
    }

    @Override
    public void onHostDestroy() {
        if (mReactContext != null) {
            mReactContext.removeLifecycleEventListener(this);
        }
        if (mGiftPlayerView != null) {
            mGiftPlayerView.onVideoDestroy();
        }
    }
}
