package com.giftplayer.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.giftplayer.R;
import com.yy.yyeva.EvaAnimConfig;
import com.yy.yyeva.inter.IEvaAnimListener;
import com.yy.yyeva.view.EvaAnimView;

import java.io.File;

public class GiftPlayerView extends FrameLayout implements IEvaAnimListener {

    private EvaAnimView mEvaAnimView;

    public GiftPlayerView(@NonNull Context context) {
        super(context);
        initView(context);
    }

    public GiftPlayerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.gift_player_view, this, true);
        mEvaAnimView = findViewById(R.id.gift_player_view);
    }

    private final Runnable measureAndLayout = new Runnable() {
        @Override
        public void run() {
            measure(
                    MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
            layout(getLeft(), getTop(), getRight(), getBottom());
        }
    };

    @Override
    public void requestLayout() {
        super.requestLayout();
        post(measureAndLayout);
    }

    /**
     * 开始播放
     *
     * @param path
     */
    public void startPlay(String path) {
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            if (!file.exists()) {
                return;
            }
            if (mEvaAnimView != null) {
                mEvaAnimView.startPlay(file);
            }
        }
    }

    /**
     * 停止播放
     */
    public void stopPlay() {
        if (mEvaAnimView.isRunning()) {
            mEvaAnimView.stopPlay();
        }
    }

    @Override
    public void onFailed(int i, @Nullable String s) {

    }

    @Override
    public void onVideoComplete() {

    }

    @Override
    public boolean onVideoConfigReady(@NonNull EvaAnimConfig evaAnimConfig) {
        return true;
    }

    @Override
    public void onVideoDestroy() {

    }

    @Override
    public void onVideoRender(int i, @Nullable EvaAnimConfig evaAnimConfig) {

    }

    @Override
    public void onVideoRestart() {

    }

    @Override
    public void onVideoStart() {

    }
}
