// ReactNativeGiftPlayerModule.java

package com.giftplayer;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;

public class GiftPlayerModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public GiftPlayerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "ReactNativeGiftPlayer";
    }

}
